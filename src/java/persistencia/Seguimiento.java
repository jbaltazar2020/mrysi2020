/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;
import java.util.Date;
/**
 *
 * @author jbalt
 */
public class Seguimiento {
	private Integer seguimiento_id;
	private Integer solicitud_id;
	private Date fechaSeguimiento;
	
	public Seguimiento(Integer seguimiento_id, Integer solicitud_id, Date fechaSeguimiento){
		this.seguimiento_id = seguimiento_id;
		this.solicitud_id = solicitud_id;
		this.fechaSeguimiento = fechaSeguimiento;
	}  
}
