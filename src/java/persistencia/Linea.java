/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

/**
 *
 * @author jbalt
 */
public class Linea {
	private Integer linea_id;
	private Integer equipo_id;
	private Integer numero;
	private Integer plan_id;
	private Integer meses_contrato;
	private Integer estatus;
	
	public Linea(Integer linea_id, Integer equipo_id, Integer numero, Integer plan_id, Integer meses_contrato, Integer estatus){
		this.linea_id = linea_id;
		this.equipo_id = equipo_id;
		this.numero = numero;
		this.plan_id = plan_id;
		this.meses_contrato = meses_contrato;
		this.estatus = estatus;
	}
}
