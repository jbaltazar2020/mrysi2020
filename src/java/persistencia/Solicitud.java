/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.util.Date;

/**
 *
 * @author jbalt
 */
public class Solicitud {
	private Integer solicitud_id;
	private Integer linea_id;
	private Date fechaSolicitud;
	
	public Solicitud(Integer solicitud_id, Integer linea_id){
		this.solicitud_id = solicitud_id;
		this.linea_id = linea_id;
	}
	
	public Integer getSolicitud_id() {
        return this.solicitud_id;
    }
	
	public void setSolicitud_id(Integer solicitud_id) {
        this.solicitud_id = solicitud_id;
    }
}
