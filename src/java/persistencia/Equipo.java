/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;
import java.util.Date;
/**
 *
 * @author jbalt
 */
public class Equipo {
	private Integer equipo_id;
	private Integer linea_id;
	private Date fechaSolicitud;
	
	public Equipo(Integer equipo_id, Integer linea_id){
		this.equipo_id = equipo_id;
		this.linea_id = linea_id;
	}    
}
