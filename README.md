SISTEMA ASOLRED
===============

Descripción del proyecto ASOLRED
--------------------------------

El sistema ASOLRED es un modulo para que las personas con una linea de telefonia movil puedan solicitar un 
cambio sobre el equipo de telefonía de dicha linea. Cambios de equipo de telefonía movil por falla, por 
vencimiento de plan. Suspension  o activacion de la linea.

El departamento  de Operaciones y Servicios internos de Consorcio Peredo a cuenta con más de 2300 contratos
de líneas empresariales para comunicación inmediata entre el personal administrativo de sucursales, gerentes
de sucursal y regionales   (Supervisor de varias sucursales), y en las oficinas corporativas así como líneas
de red para jefes de departamento y supervisores de dichos departamentos, para el uso de los dispositivos
electrónicos que tienen asignados como tabletas, celulares, etc.

Este departamento es el encargado de solicitar y pagar los servicios de telefonía móvil (líneas de red) a las compañías proveedoras del servicio como Telcel, Movistar, etc. Asi mismo es el encargado de solicitar cambios de equipo por robo o vencimiento del plan, revisión por fallas, cambio de lada y chip, suspensión o reactivación del servicio.


El sistema ASOLRED deberá ser capaz de generar solicitudes de cambio de estatus para cada línea de las
empresas que componen el Consorcio Peredo, esto se realizara desde el sistema correspondiente a cada empresa.
Al generarse una solicitud, se deberá enviar correo generado por sistema al socio con puesto superior para
que ingrese a sistema para autorizar o rechazar en sistema la solicitud.

Si la solicitud es autorizada por jefe superior que puede ser gerente regional o jefe de departamento, se
debe enviar correo por sistema, al departamento de Administración de cada empresa para que de igual forma
autorice o rechace la solicitud.

De igual forma si la solicitud es autorizada por jefe de Departamento de Administración de la empresa, se 
informará por correo generado por sistema a jefe del departamento de Operaciones.

En la bandeja entrada o reporte de solicitudes que ya están autorizadas por jefe Administración de la 
empresa, el jefe de operaciones debe poder generar un grupo por tipo de solicitud y generarse un correo a la
compañía a la que pertenezca la línea.

Cuando se tenga respuesta por parte de la compañía proveedora del servicio, en los casos de cambio de equipo
se deberá capturar según corresponda el nuevo número de chip, imei, marca, modelo y costo del equipo. Y 
estos cambios debe replicase en el sistema de la empresa a la que pertenece la línea de red.

En los casos de vencimiento de plan, se deben capturar las nuevas fechas de vigencia, plazo y costo del plan 
y replicar los cambios en el sistema de la empresa correspondiente.

